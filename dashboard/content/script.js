const URL = 'http://192.168.0.20:5000/api'

function refreshLoop() {
    // console.log('Refreshing Statistics')
    
    connectionCheck()
    getAll('/logs', 'logTable')

    getCount('/users/count', 'userCount')
    getCount('/zones/count', 'zoneCount')
    getCount('/groups/count', 'groupCount')
    getCount('/doors/count', 'doorCount')
    getCount('/entries/today', 'entryCountToday')
    getCount('/entries/unique/week', 'uniqueEntriesWeek')
    getCount('/entries/unique/today', 'uniqueEntriesToday')
    getCount('/entries/rejected/today', 'rejectedEntryCountToday')
    
}

function doGet(endpoint) {
    return fetch(URL + endpoint, {method:'GET'})
        .then(response => response.json())
        .catch(err => console.log('Get Error '+ err))
}

function doDelete(endpoint, data) {
    return fetch(URL + endpoint, {
        method:'DELETE', 
        body:JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(response => response.json())
        .catch(err => console.log('Post Error '+ err))
}

function doPost(endpoint, data) {
    return fetch(URL + endpoint, {
        method:'POST', 
        body:JSON.stringify(data),
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(response => response.json())
        .catch(err => console.log('Post Error '+ err))
}

function connectionCheck() {
    const connectionText = document.getElementById("connectionStatus")
    doGet('/heartbeat') 
        .then ((data) => { if (data.connection) {
            connectionText.classList.remove('w3-text-red')
            connectionText.classList.add('w3-text-green')
            connectionText.innerHTML = "ONLINE"
        }}).catch(() => {
            connectionText.classList.remove('w3-text-green')
            connectionText.classList.add('w3-text-red')
            connectionText.innerHTML = "OFFLINE"
        })
}

function getAll(endpoint, id) {
    const table = document.getElementById(id)
    table.innerHTML=''
    doGet(endpoint).then(item => item.forEach(item => {
        const newRow = document.createElement('tr')

        item.forEach(value => {
            const cell = document.createElement('td')
            cell.innerText = value
            newRow.appendChild(cell)
        })
        
        table.appendChild(newRow)
    }))
}

function getCount(endpoint, id) {
    const element = document.getElementById(id)
    doGet(endpoint).then(result => 
        element.innerText = result[0][0]
    ).catch((endpoint) => console.log('Endpoint Failure:' + endpoint))
}

function addFormData(endpoint, formID) {
    var formData = {}
    const form = document.forms[formID].getElementsByTagName('input')
    for (var item of form) {
        const rowid = item.id
        const rowval = item.value
        formData[rowid] = rowval
    }
    doPost(endpoint, formData)
}

function deleteFormData(endpoint, formID) {
    var formData = {}
    const form = document.forms[formID].getElementsByTagName('input')
    for (var item of form) {
        const rowid = item.id
        const rowval = item.value
        formData[rowid] = rowval
    }
    doDelete(endpoint, formData)
}

function getUserCode() {
    var userID = document.getElementById('user_id_code').value
    window.open(URL+'/users/qr/'+userID, '_blank', 'toolbar=0,location=0,menubar=0')
}

refreshLoop()
setInterval(refreshLoop, 5000)