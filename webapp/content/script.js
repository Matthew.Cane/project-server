const URL = 'http://192.168.0.20:5000/api'

function generateTOTP(key){
	return performHMAC(key, getTimeStep())
}

function performHMAC(key, message) {
	const hmacObj = new jsSHA('SHA3-256', 'TEXT')

	hmacObj.setHMACKey(key, 'TEXT')
	hmacObj.update(message + '')
	return hmacObj.getHMAC('HEX')

}

function getTimeStep() {
	return Math.round(Date.now()/10000)*10
}

function setUserData (UID, SECRET, NAME) {
	localStorage.setItem('UID', UID)
	localStorage.setItem('SECRET', SECRET)
	localStorage.setItem('NAME', NAME)
}

function getUserToken() {
	return localStorage.getItem('UID')+generateTOTP(localStorage.getItem('SECRET'))
	
}

function generateQRCode(data) {

	if (screen.width > screen.height) {
		var width = screen.height - screen.height * 20 / 100
	} else {
		var width = screen.width - screen.width * 20 / 100
	}
	document.getElementById('qrcode').innerHTML = ''
	new QRCode("qrcode", {
		text: data,
		width: width,
		height: width
	})
}

function refreshLoop() {
	console.log('Generating new code')
	generateQRCode(getUserToken());
}

function resetID() {
	localStorage.clear();
	window.location = "http://192.168.0.20:81"
}

if (!localStorage.getItem('UID')) {
	const hashParams = window.location.hash.substring(1)
	const params = hashParams.split('&')
	const uid = params[0].split('=')[1]
	const secret = params[1].split('=')[1]
	const name = params[2].split('=')[1].replace('%20', ' ')

	setUserData(uid, secret, name)
}


document.getElementById('name').innerText = localStorage.getItem('NAME')
generateQRCode(getUserToken());
setInterval(refreshLoop, 10000)

