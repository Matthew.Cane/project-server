from flask import Flask, send_file
from flask_restful import Api, Resource, reqparse
from string import ascii_letters
from hashlib import sha3_256
from flask_cors import CORS
from io import BytesIO
from PIL import Image
import sys
import qrcode
import hmac
import secrets
import datetime
import time
import database
import json

def logEvent(eventType, message):
	logMessage = "{}\t[{}]\t{}\n".format(getDateTime(), eventType.upper(), message)
	print(logMessage[:-1], flush=True) #[:-1] strips endline for printing
	# Output log to DB here

def getDateTime():
	return datetime.datetime.now().replace(microsecond=0).isoformat()

def getTimetamp():
	return int(time.time())

def getSHA3(string, returnType):
	if returnType == "hex":
		return sha3_256(bytes(string, "ascii")).hexdigest()
	elif returnType == "bytes":
		return sha3_256(bytes(string, "ascii")).digest()

def generateBase64String(length):
	ID = ""
	characters = ascii_letters + "+" + "/"
	for i in range (0,10):
		characters += str(i)
	for i in range(0,length):
		ID += secrets.choice(characters)
	return ID

def generateRandomHex(length):
	ID = ""
	characters = ascii_letters[:6].upper()
	for i in range (0,10):
		characters += str(i)
	for i in range(0,length):
		ID += secrets.choice(characters)
	return ID

def performHMAC(key, message):
	b_key = bytes(key, 'ascii')
	b_message = str(message).encode('ascii')
	h = hmac.new(b_key, b_message, sha3_256)
	return h.hexdigest()

def generateTOTP(key):
	timestep = -1
	TS = round(getTimetamp(), timestep)
	calculatedToken = performHMAC(key, TS)
	return calculatedToken

def authorised(args):
	suppliedToken = args["UKEY"]
	userSecret = db.getUserSecret(args["UID"])
	generatedToken = generateTOTP(userSecret)

	if generatedToken == suppliedToken:
		return True

	return False

def generateQRCode(user_id):
	secret = db.getUserSecret(user_id)
	name = "{0} {1}".format(db.getUserInfo(user_id)[1], db.getUserInfo(user_id)[2])
	content = "http://192.168.0.20:81/#uid={0}&secret={1}&name={2}".format(user_id, secret, name)
	pilImage = qrcode.make(content)
	path = "/tmp/qr"+user_id
	pilImage.save(path, format="PNG")
	return path

#-------------------------------
# START OF RESOURCE CLASSES
#-------------------------------

class heartbeat(Resource):
    def get(self):
        return {"connection": True}

class testTrue(Resource):
    def get(self):
        return {"data": True}

class testFalse(Resource):
    def get(self):
        return {"data": False}

class apiAuth(Resource):
	def post(self):
		args = authArgsParser.parse_args()
		if not authorised(args):
			return {"authorised": False}
		return {"authorised": True}

class apiLogin(Resource):
	def post(self):
		args = loginArgsParser.parse_args()
		if (args.user_name == "admin" and args.password == "password"):
			return {"authorised": True}
		else:
			return {"authorised": False}

class apiLogs(Resource):
	def post(self):
		args = logsArgsParser.parse_args()
		db.logEvent(args.door_id, args.user_id, getTimetamp(), args.zone_authorised, args.key_authorised)
		return None
	
	def get(self):
		return db.getAllLogsFormatted()

class apiUsers(Resource):
	def get(self):
		return db.getAllUsers()

	def delete(self):
		args = deleteUserArgsParser.parse_args()
		db.deleteUser(args.user_id)
		return {"user_deleted":args.user_id}
	
	def post(self):
		args  = userArgsParser.parse_args()
		UID = generateBase64String(64)
		secret = generateBase64String(128)
		db.addUser(UID, args.first_name, args.last_name, secret)
		return {"user_added":UID}

class apiUsersCount(Resource):
	def get(self):
		return db.getUserCount()

class apiQR(Resource):
	def get(self, user_id):
		return send_file(generateQRCode(user_id),
			mimetype="image/png",
			as_attachment=False,
			attachment_filename="code.png")

class apiZones(Resource):
	def get(self):
		return db.getAllZones()

	def post(self):
		args = zoneArgsParser.parse_args()
		db.addZone(args.name, args.location, args.description, args.is_inside)
		return {"Success":True}

class apiZonesCount(Resource):
	def get(self):
		return db.getUserCount()

class apiGroups(Resource):
	def get(self):
		return db.getAllGroups()

	def post(self):
		args = groupArgsParser.parse_args()
		db.addGroup(args.name, args.description)

class apiGroupsCount(Resource):
	def get(self):
		return db.getGroupCount()

class apiUserGroups(Resource):
	def post(self):
		args = userGroupArgsParser.parse_args()
		db.addUserToGroup(args.user_id, args.group_id)
		return {"Success":True}

class apiDoors(Resource):
	def get(self):
		return db.getAllDoors()

class apiDoorsCount(Resource):
	def get(self):
		return db.getDoorCount()

class apiEntries(Resource):
	def get(self):
		return None

class apiEntriesToday(Resource):
	def get(self):
		return db.getUniqueEntriesCount(0)

class apiEntriesUniqueToday(Resource):
	def get(self):
		return db.getUniqueEntriesCount(86400)

class apiEntriesUniqueWeek(Resource):
	def get(self):
		return db.getUniqueEntriesCount(604800)

class apiEntriesRejectedToday(Resource):
	def get(self):
		return db.getRejectedEntriesCount(86400)

#-------------------------------
# START OF SYSTEM INIT
#-------------------------------

app = Flask(__name__)
CORS(app)
api = Api(app)
db = database.dbQuery("/db/securitySystem.db")
#-------------------------------
# ARGPARSE INIT
#-------------------------------

authArgsParser = reqparse.RequestParser()
authArgsParser.add_argument("UID", type=str, required=True)
authArgsParser.add_argument("UKEY", type=str, required=True)
authArgsParser.add_argument("DID", type=str, required=True)

loginArgsParser = reqparse.RequestParser()
loginArgsParser.add_argument("user_name", type=str, required=True)
loginArgsParser.add_argument("password", type=str, required=True)

userArgsParser = reqparse.RequestParser()
userArgsParser.add_argument("first_name", type=str, required=True)
userArgsParser.add_argument("last_name", type=str, required=True)

deleteUserArgsParser = reqparse.RequestParser()
deleteUserArgsParser.add_argument("user_id", type=str, required=True)

groupArgsParser = reqparse.RequestParser()
groupArgsParser.add_argument("name", type=str, required=True)
groupArgsParser.add_argument("description", type=str, required=True)


zoneArgsParser = reqparse.RequestParser()
zoneArgsParser.add_argument("name", type=str, required=True)
zoneArgsParser.add_argument("description", type=str, required=True)
zoneArgsParser.add_argument("location", type=str, required=True)
zoneArgsParser.add_argument("is_inside", type=str, required=True)

userGroupArgsParser = reqparse.RequestParser()
userGroupArgsParser.add_argument("user_id", type=str, required=True)
userGroupArgsParser.add_argument("group_id", type=str, required=True)

logsArgsParser = reqparse.RequestParser()
logsArgsParser.add_argument("door_id", type=str, required=True)
logsArgsParser.add_argument("user_id", type=str, required=True)
logsArgsParser.add_argument("zone_authorised", type=bool, required=True)
logsArgsParser.add_argument("key_authorised", type=bool, required=True)

#-------------------------------
# API RESOURCE INIT
#-------------------------------

# Test resources
api.add_resource(heartbeat, "/api/heartbeat")
api.add_resource(testTrue, "/api/test/true")
api.add_resource(testFalse, "/api/test/false")

#
api.add_resource(apiAuth, "/api/auth")
api.add_resource(apiLogs, "/api/logs")
api.add_resource(apiLogin, "/api/login")

# Users
api.add_resource(apiUsers, "/api/users")
api.add_resource(apiUsersCount, "/api/users/count")
api.add_resource(apiQR, "/api/users/qr/<string:user_id>")

api.add_resource(apiZones, "/api/zones")
api.add_resource(apiZonesCount, "/api/zones/count")

api.add_resource(apiGroups, "/api/groups")
api.add_resource(apiGroupsCount, "/api/groups/count")
api.add_resource(apiUserGroups, "/api/groups/adduser")

api.add_resource(apiDoors, "/api/doors")
api.add_resource(apiDoorsCount, "/api/doors/count")


api.add_resource(apiEntries, "/api/entries")
api.add_resource(apiEntriesToday, "/api/entries/today")
api.add_resource(apiEntriesUniqueToday, "/api/entries/unique/today")
api.add_resource(apiEntriesUniqueWeek, "/api/entries/unique/week")
api.add_resource(apiEntriesRejectedToday, "/api/entries/rejected/today")
#-------------------------------
# END OF SYSTEM INIT
#-------------------------------


if __name__ == "__main__":
	app.run(host="0.0.0.0") # ssl_context='adhoc'
