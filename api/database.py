import sqlite3
import os.path
import time

class dbQuery():
	path = None

	def __init__(self, dbPath):
		self.path = dbPath

	def getCursor(self):
		if os.path.exists(self.path):
			DBconnection = sqlite3.Connection(self.path, isolation_level=None)
			return DBconnection.cursor()
		else:
			DBconnection = sqlite3.Connection(self.path, isolation_level=None)
			initScript = open("dbInitScript.sql").read()
			cursor = DBconnection.cursor()
			cursor.executescript(initScript)
			return cursor

	#-------------------------------
	# USER QUERIES
	#-------------------------------

	# POST /api/users/
	def addUser(self, UID, FN, LN, secret):
		cursor = self.getCursor()
		cursor.execute("INSERT INTO User VALUES (?,?,?,?)",(UID,FN,LN,secret))

	# 
	def getUserSecret(self, UID):
		cursor = self.getCursor()
		cursor.execute("SELECT secret FROM User WHERE user_id=?",(UID,))
		values = cursor.fetchone()
		if len(values) != 1:
			return False
		return values[0]

	def getUserInfo(self, UID):
		cursor = self.getCursor()
		cursor.execute("SELECT user_id, first_name, last_name FROM User WHERE user_id=?",(UID,))
		values = cursor.fetchone()
		return values

	# GET /api/users
	def getAllUsers(self):
		cursor = self.getCursor()
		cursor.execute("SELECT user_id, first_name, last_name FROM User")
		values = cursor.fetchall()
		return values

	# GET /api/users?q=not+group+<id>
	# GET /api/users?not_group=<id>
	def getUsersNotInGroup(self):
		query = """
			SELECT first_name, last_name
			FROM User WHERE user_id
			NOT IN (SELECT user_id FROM User_Group)
		"""
		cursor = self.getCursor()
		cursor.execute(query)
		values = cursor.fetchall()
		return values

	def getUserCount(self):
		cursor = self.getCursor()
		cursor.execute("SELECT COUNT(user_id) FROM User")
		values = cursor.fetchall()
		return values

	def deleteUser(self, user_id):
		cursor = self.getCursor()
		cursor.execute("DELETE FROM User WHERE user_id=?", (user_id,))

	#-------------------------------
	# GROUP QUERIES
	#-------------------------------

	# GET /api/users?group=<id>
	# GET /api/groups/<id>/users
	def usersInGroup(self, group):
		users = []
		cursor = self.getCursor()
		cursor.execute("SELECT user_id FROM User_Group WHERE group_id=?",(group,))
		IDList = cursor.fetchall()
		for user in IDList:
			users.append(self.GetUserInfo(user))
		return users

	# GET /api/groups
	def getAllGroups(self):
		cursor = self.getCursor()
		cursor.execute("SELECT * FROM 'Group'")
		values = cursor.fetchall()
		return values

	def getGroupCount(self):
		cursor = self.getCursor()
		cursor.execute("SELECT COUNT(group_id) FROM 'Group'")
		values = cursor.fetchall()
		return values
	
	def addGroup(self, name, description): 
		cursor = self.getCursor()
		cursor.execute("INSERT INTO 'Group' (name, description) VALUES (?,?)",(name, description))
		
	def addUserToGroup(self, user_id, group_id):
		cursor = self.getCursor()
		cursor.execute("""
			INSERT INTO User_Group (user_id, group_id)
			VALUES (?,?)""", (user_id, group_id))

	def deleteGroup(self, group_id):
		cursor = self.getCursor()
		cursor.execute("DELETE FROM Group WHERE user_id=?", (group_id,))

	def deleteUserFromGroup(self, user_id, group_id):
		cursor = self.getCursor()
		cursor.execute("DELETE FROM User_Group WHERE user_id=? AND Group_id=?", (user_id, group_id))

	#-------------------------------
	# ZONE QUERIES
	#-------------------------------

	def getAllZones(self):
		cursor = self.getCursor()
		cursor.execute("""
							SELECT z.zone_id, z.name, z.location, z.description, y.name
							FROM Zone z
							LEFT JOIN Zone y ON z.is_inside = y.zone_id
						""")
		values = cursor.fetchall()
		return values

	def getParentZones(self):
		cursor = self.getCursor()
		cursor.execute("SELECT * FROM Zone WHERE is_inside IS NULL")
		values = cursor.fetchall()
		return values

	def getChildZones(self, zone_id):
		cursor = self.getCursor()
		cursor.execute("SELECT * FROM Zone WHERE is_inside =?",(zone_id,))
		values = cursor.fetchall()
		return values

	def doorsInZone(self, doorID):
		cursor = self.getCursor()
		cursor.execute("SELECT door_id FROM Door WHERE zone_id = ?",(doorID,))
		values = cursor.fetchall()
		return values

	def getZoneCount(self):
		cursor = self.getCursor()
		cursor.execute("SELECT COUNT(zone_id) FROM Zone")
		values = cursor.fetchall()
		return values

	def addZone(self, name, location, description, is_inside):
		cursor = self.getCursor()
		if is_inside:
			cursor.execute("""
				INSERT INTO Zone 
				(name, location, description, is_inside)
				VALUES (?,?,?,?)""",(name, location, description, is_inside))
		else:
			cursor.execute("""
				INSERT INTO Zone 
				(name, location, description)
				VALUES (?,?,?)""",(name, location, description))
		
	def deleteZone(self, zone_id):
		cursor = self.getCursor()
		cursor.execute("DELETE FROM Zone WHERE zone_id=?", (zone_id,))

	#-------------------------------
	# DOOR QUERIES
	#-------------------------------

	def getAllDoors(self):
		query = """
			SELECT D.door_id, Z.name AS Zone
			FROM Door AS D
			LEFT JOIN Zone AS Z
			ON D.zone_id = Z.zone_id;
		"""
		cursor = self.getCursor()
		cursor.execute(query)
		values = cursor.fetchall()
		return values

	def addDoor(self, doorID):
		cursor = self.getCursor()
		cursor.execute("INSERT INTO door (door_id) VALUES (?)",(doorID,))

	def isDoor(self, doorID):
		doorList = self.getAllDoors()
		for door in doorList:
			if doorID in door:
				return True
		return False

	def getDoorCount(self):
		cursor = self.getCursor()
		cursor.execute("SELECT COUNT(door_id) FROM Door")
		values = cursor.fetchall()
		return values

	#-------------------------------
	# LOG QUERIES
	#-------------------------------

	def getAllLogs(self):
		cursor = self.getCursor()
		cursor.execute("SELECT * FROM Event_Log")
		values = cursor.fetchall()
		return values

	def getAllLogsFormatted(self):
		cursor = self.getCursor()
		cursor.execute("""
			SELECT
				Z_D.name AS 'Zone Name',
				User.first_name || ' ' || User.last_name AS 'User', 
				Event_Log.zone_authorised,
				Event_Log.key_authorised,
				datetime(time_stamp, 'unixepoch') AS 'Date/Time'
			FROM Event_Log
			LEFT JOIN User 
				ON User.user_id = Event_Log.user_id
			LEFT JOIN (SELECT Door.door_id, Zone.name
				FROM Door LEFT JOIN Zone ON Zone.Zone_id = Door.zone_id) AS Z_D
				ON Z_D.door_id = Event_Log.door_id
			ORDER BY Event_Log.time_stamp DESC
		""")
		values = cursor.fetchall()
		return values

	def logEvent(self, door_id, user_id, time_stamp, zone_authorised, key_authorised):
		query = """
			INSERT INTO Event_Log (door_id, user_id, time_stamp, zone_authorised, key_authorised)
			VALUES (?,?,?,?,?)
		"""
		cursor = self.getCursor()
		cursor.execute(query, (door_id, user_id, time_stamp, zone_authorised, key_authorised))

	def getUniqueEntriesCount(self, timeRange):
		# Time range in seconds from now
		cursor = self.getCursor()
		cutoffTime = int(time.time()) - timeRange
		cursor.execute("SELECT COUNT(user_id) FROM (SELECT DISTINCT user_id FROM Event_Log WHERE time_stamp < ?)", (cutoffTime,))
		values = cursor.fetchall()
		return values

	def getRejectedEntriesCount(self, timeRange):
		# Time range in seconds from now
		cursor = self.getCursor()
		cutoffTime = int(time.time()) - timeRange
		cursor.execute("""
			SELECT COUNT(user_id) FROM 
			(SELECT DISTINCT user_id FROM Event_Log 
			WHERE time_stamp < ? AND zone_authorised 
			OR key_authorised = 0)""", (cutoffTime,))
		values = cursor.fetchall()
		return values