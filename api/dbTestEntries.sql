-- TEST ENTRIES

-----------------------------
-- USERS
-----------------------------

INSERT INTO User VALUES (
    "SnFLOwdkedAhJnccBEUvWN1hdhvfiCL4FZXQbXl8RRySTx7gXqyO9AEMpXTuaeVk",
    "Matt",
    "Cane",
    "ntK7Rs5R1DRTEXibN9gnGjNRImiGTqWKviWjtGnUniVL83PwFyv9IBPUKbcU6OKz5XfeOCKsdiQKkFaNzrnt0460nMPsaHteK9BeUQlOvHVYuhfly4MkUtrAc38G4aEO"
);


INSERT INTO User VALUES (
    "zCMSccPpEdxlxvb8Gdcrk0lBIflpMwJaOq9WCCVe6JpweqoBfGmgoP3MxCKF03cs",
    "Paula",
    "Foreman",
    "VdbMyGd9e7FkWaOKsbA6BC9Ig6ayGP7gy4H8x9Y9MVz0BmadoD1FwhurzdldbXe2J0bbkO68XEktDh9XNtHewvhCBd3aKl61246klFbEngo1Iddl1Wyrvg1X7dyZjvsh"
);


INSERT INTO User VALUES (
    "McA3S9ocvGOKeRGUMrgtuiITWEnbeTkM1AY85bYCQpQDDB7uPhG59Be2sOscdLjz",
    "Dominic",
    "Meredith",
    "Ou73j90xaLSwqesJMVGn2kxPGQ74mVgz1jyvNjfVgG249lyeLlHyAP9KmIWMjZ1gzL0Tlhz4wZtzYK740e5iWFZ5dCgX2Vke8qA0pB3dXU7m0JRAnjBqeHmu8F4euKvk"
);


INSERT INTO User VALUES (
    "GIOSpjzlI8RPH9qnCL3j1EKe09yQJemquQqNAmps23kNsvhe0tsXcu0vDhbx119k",
    "Owen",
    "Crook",
    "L1eI9mrmxrVIPIb4tapMb77I4EGhxlkkaHwmDRPbuc5J61VCbpFDEA8Vu3Vb55i7fJzZkPYkXk18kFTZJrJugQM5qeyM8AviS6qRhaOtxpfl1U8Cz1jn6Wh6EIolQlcS"
);


INSERT INTO User VALUES (
    "3mxm8IHRYIf51HA1yKUIaW8LzfV69lrFd5yOpj9rs4jDVKvSXIdP13TT37lQWwuS",
    "Conor",
    "Mcsherry",
    "5FpzRaf3opnlnR3GnzxinEo4f8db0CTRFGmG6uvkIe4d2qcTtCqbbYB3MCstNU8mkmaKfwVqjqefI261c6ZjmFeV4vjXp4MyP2yXR0OE6xQtRS1mtAbx48gBFuygzkYI"
);


INSERT INTO User VALUES (
    "L1LRX7MFRM0vNCvrDRxtJX29n648Q0ax5YjrImMDCaPgy6xKwWZj6vsB4UkDJ7gz",
    "Nicola",
    "Divers",
    "byZF0pMrL9hWgWznva0M4hmYj1DpSdroHuyJLw7e5RNlomzihXi96256zxL6nTjcD5eNGhx55NAlXIqO2GAPaqgbVkFfClRSIcuRvMLbw2FY3pHtamEu27mGxEIclmmh"
);


INSERT INTO User VALUES (
    "yDSEflfag6jAQ4280sVn4hrjhZofohq3v9afvG7klKV7zqBOVNi63TTSaZGt5NjG",
    "Ethan",
    "O'Neil",
    "rc7gjtYCSFM5VaJZsKEisOWp7t0Rr9r1cpfehPFRzEljh8hdbUt8QSxyaHZvRfMGRKgYXlwLiOrtq5FZBMSmYp1JjaNoF0ltpAlGhqoszWzTOfjhYfeShH2hRhgtmay0"
);


INSERT INTO User VALUES (
    "rL1ZmttS2owHmqJsLIJCbJZKkVpwyIOjnMVkzrgx3FTSOI9v6X54fFA2KF4t2hXN",
    "Bruce",
    "Lundie",
    "meo6Pq7PG3eC17Ie3VgWEn2lamwqHNgboajlc6WjYNdMFAEhmyqdVxUVMlSLBpQopMBd7U1lDOXJIyA6601PH0Kbp6qeEu3VuPPVwbOKlTS5QOW433Hwi90HJfV7ZLgm"
);


INSERT INTO User VALUES (
    "sUr0qZaxMxvfyXej8ioiNc5LWGoPaw3Jt2y4WIjvlGMB4bl4MOQ0xxWQ5APSdEPq",
    "Josh",
    "O'Hara",
    "EZFxssuRJLp6wPBY7oSk9eV5kJJElRzqA6I7j9T3yzkZ6xG1whYXB7Wf65zLrGmiuoh55o3aQBn4SqFmzSZWENOBi8iW6nFi4yjXH3lkp31UeCi6JOHh3OhrDwxAyvuC"
);


INSERT INTO User VALUES (
    "xTQXbBdSdlDASMqfDTrGnehBquVDvQmVHvwx3Z3QmeYhk9DFlFwrjyEPlOmOllcU",
    "Niamh",
    "Mcgougan",
    "w6Vx1v1nDPFgFEF8aY4VC9WqLRzTmi1nd7IIuIALnmcKM1DVMhk7EWzuioztp0XpIdyHNzEBQcm0r5xJY3DhBpg4DdTSloH9li1BINqpl2EW6M9TldKIWHvBANjRhksh"
);


INSERT INTO User VALUES (
    "YjeMFNIpyHneCHWTUbmM0p7mGsu8iOlxGu661KR95Spx4QHNuQ7KlG4l4GAKnPZq",
    "Bruce",
    "Hogan",
    "S3IKhMxdAJpKErjKztUQ3cNJ6jmWcJov7pbu82HJnYjo7uYC0dwkW6bGMitKUsiIhU9rHK2VRXybzYUzPrEfyc7YuUh2jjPluNVl73jUUrs2507oWyGWiE732wy14ryg"
);
-----------------------------
-- GROUPS
-----------------------------
INSERT INTO `Group` (name, description) VALUES (
    "Student",
    "Student group"
);

INSERT INTO `Group` (name, description) VALUES (
    "Teaching Staff",
    "General teaching staff"
);

INSERT INTO `Group` (name, description) VALUES (
    "Admin Staff",
    "Administration staff"
);

INSERT INTO `Group` (name, description) VALUES (
    "Visitor",
    "Site visitors"
);

INSERT INTO `Group` (name, description) VALUES (
    "Cleaning Staff",
    "Cleaning staff"
);

-----------------------------
-- ZONES
-----------------------------

INSERT INTO Zone (name, location, description) VALUES (
    "Campus",
    "University Road",
    "Main university campus"
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Science Block",
    "Building 3",
    "Science teaching rooms",
    (SELECT zone_id FROM Zone WHERE name = "Campus")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "English Block",
    "Building 2",
    "English teaching rooms",
    (SELECT zone_id FROM Zone WHERE name = "Campus")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Computing Block",
    "Building 1",
    "Computing teaching rooms",
    (SELECT zone_id FROM Zone WHERE name = "Campus")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Geology Block",
    "Building 1",
    "Geology teaching rooms",
    (SELECT zone_id FROM Zone WHERE name = "Campus")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Admin Block",
    "Building 0",
    "Admin area",
    (SELECT zone_id FROM Zone WHERE name = "Campus")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Computer Lab 1",
    "Computing Block",
    "Computing lab",
    (SELECT zone_id FROM Zone WHERE name = "Computing Block")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Science Lab 1",
    "Computing Block",
    "Science lab",
    (SELECT zone_id FROM Zone WHERE name = "Science Block")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Science Lab 2",
    "Computing Block",
    "Science lab",
    (SELECT zone_id FROM Zone WHERE name = "Science Block")
);

INSERT INTO Zone (name, location, description, is_inside) VALUES (
    "Chemical Locker 1",
    "Science Lab 1",
    "Chemical Locker",
    (SELECT zone_id FROM Zone WHERE name = "Science Lab 1")
);

-----------------------------
-- DOORS
-----------------------------

INSERT INTO Door VALUES (
    "43Z65QLzn9hMuHetTasPNsbfXt2wUP7tCmWoyhbqR88OMZptBdjqcsytoORsrpDj",
    (SELECT zone_id FROM Zone WHERE name = "Science Block")
);

INSERT INTO Door VALUES (
    "c2foHcvXNjS1NCz9Kmt5qJDVouUO6cTsaVC9dJjYTU4rsZ2jdI8X0H2pN3RjE8fa",
    (SELECT zone_id FROM Zone WHERE name = "Science Block")
);

INSERT INTO Door VALUES (
    "Dd1kfDbFUQhsog6qjilS0jQNHSfZpUXF733T56KsJzgPQz4393zEcOzFl1PxoueN",
    (SELECT zone_id FROM Zone WHERE name = "English Block")
);

INSERT INTO Door VALUES (
    "i8FZ95MkXV4c6IsNS1tSFDA8jaFnR2yuaXypAlRI35i1rMrxVw6gCxcK94jFrOKJ",
    (SELECT zone_id FROM Zone WHERE name = "English Block")
);

INSERT INTO Door VALUES (
    "opz3ymG9mttFgZ9a0j4h6f8abdHsaZSRdyyAMcwqUZyr3Be7ec2gv0iGInjGAsog",
    (SELECT zone_id FROM Zone WHERE name = "Computing Block")
);

INSERT INTO Door VALUES (
    "LITNC8LmlnqkrcD1J3bwnm60r6TzNLEncGel8SN6P8us0HEqmztAtPzB1GAfq6XG",
    (SELECT zone_id FROM Zone WHERE name = "Geology Block")
);

INSERT INTO Door VALUES (
    "Rg25pcXyCOzhEOYtPqfgu6jkuLzjcLV3vJNaHtsm5QIbGZ6kLUmJqaVCvuyzFk1Q",
    (SELECT zone_id FROM Zone WHERE name = "Admin Block")
);

INSERT INTO Door VALUES (
    "ej1ptpnx2P0XGHEKwXS8ujYXxiNLp7520Ags4SX00aHtyTtzVSET359IgbTPl0mZ",
    (SELECT zone_id FROM Zone WHERE name = "Computer Lab 1")
);

INSERT INTO Door VALUES (
    "sIdNodK2z4iBTukJfRKxaQiUKOS4JUd50KBzXLhJfvCSIlG5wsaFjlr28ODIFuua",
    (SELECT zone_id FROM Zone WHERE name = "Science Lab 1")
);

INSERT INTO Door VALUES (
    "Pfnjvl6QBolh6dIcSFV663knP3HxYWDv8QuEgN2wpti3f9StbvgVAnSiTIoTAbHe",
    (SELECT zone_id FROM Zone WHERE name = "Science Lab 2")
);

INSERT INTO Door VALUES (
    "Z6iVLb9fAQGzOGDFi7KJeEEyxMo7AZWuTJoRf17PcuiPyAkN7Gf33jYSwHRGGWKM",
    (SELECT zone_id FROM Zone WHERE name = "Chemical Locker 1")
);

-----------------------------
-- USER_GROUP
-----------------------------

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Matt" AND last_name = "Cane"),
    (SELECT group_id FROM "Group" WHERE name = "Student")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Paula" AND last_name = "Foreman"),
    (SELECT group_id FROM "Group" WHERE name = "Student")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Dominic" AND last_name = "Meredith"),
    (SELECT group_id FROM "Group" WHERE name = "Student")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Owen" AND last_name = "Crook"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Conor" AND last_name = "Mcsherry"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Nicola" AND last_name = "Divers"),
    (SELECT group_id FROM "Group" WHERE name = "Admin Staff")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Ethan" AND last_name = "O'Neil"),
    (SELECT group_id FROM "Group" WHERE name = "Visitor")
);

INSERT INTO User_Group (user_id, group_id) VALUES (
    (SELECT user_id FROM User WHERE first_name = "Bruce" AND last_name = "Lundie"),
    (SELECT group_id FROM "Group" WHERE name = "Cleaning Staff")
);

-----------------------------
-- Group_Access
-----------------------------

INSERT INTO Group_Access (zone_id, group_id, access) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "Science Block"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff"),
    1
);

INSERT INTO Group_Access (zone_id, group_id, access) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "English Block"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff"),
    1
);

INSERT INTO Group_Access (zone_id, group_id, access) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "Computing Block"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff"),
    1
);

INSERT INTO Group_Access (zone_id, group_id, access) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "Geology Block"),
    (SELECT group_id FROM "Group" WHERE name = "Teaching Staff"),
    1
);

INSERT INTO Group_Access (zone_id, group_id, access, access_between) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "Admin Block"),
    (SELECT group_id FROM "Group" WHERE name = "Admin Staff"),
    1,
    "0900-1400"
);

INSERT INTO Group_Access (zone_id, group_id, access, expires_on) VALUES (
    (SELECT zone_id FROM Zone WHERE name = "Admin Block"),
    (SELECT group_id FROM "Group" WHERE name = "Admin Staff"),
    1,
    1633046400
);

-----------------------------
-- User_Access
-----------------------------

INSERT INTO User_Access (zone_id, user_id, access) VALUES (
	(SELECT zone_id FROM Zone WHERE name = "Chemical Locker 1"),
	(SELECT user_id FROM User WHERE first_name = "Owen" AND last_name = "Crook"),
	1
);
