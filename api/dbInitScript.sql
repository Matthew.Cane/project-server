-- CREATE TABLES

CREATE TABLE User (
    user_id TEXT UNIQUE,
    first_name TEXT NOT NULL,
    last_name TEXT NOT NULL,
    secret TEXT UNIQUE NOT NULL,
    PRIMARY KEY (user_id)
);

CREATE TABLE `Group` (
    group_id INTEGER,
    name TEXT NOT NULL,
    description TEXT,
    PRIMARY KEY (group_id)
);

CREATE TABLE User_Group (
    user_group_id INTEGER,
    user_id TEXT NOT NULL,
    group_id INTEGER NOT NULL,
    PRIMARY KEY (user_group_id),
    FOREIGN KEY (user_id) REFERENCES User(user_id),
    FOREIGN KEY (group_id) REFERENCES `Group`(group_id)
);

CREATE TABLE Zone (
    zone_id INTEGER,
    name TEXT UNIQUE NOT NULL,
    location TEXT,
    description TEXT,
    is_inside TEXT,
    PRIMARY KEY (zone_id)
    FOREIGN KEY (is_inside) REFERENCES Zone(zone_id)
);

CREATE TABLE Group_Access (
    group_access_id INTEGER,
    zone_id INTEGER NOT NULL,
    group_id INTEGER NOT NULL,
    access INTEGER NOT NULL,
    access_between TEXT,
    expires_on INTEGER,
    PRIMARY KEY (group_access_id)
    FOREIGN KEY (zone_id) REFERENCES Zone(zone_id),
    FOREIGN KEY (group_id) REFERENCES 'Group'(group_id)
);

CREATE TABLE User_Access (
    user_access_id INTEGER,
    zone_id INTEGER NOT NULL,
    user_id TEXT NOT NULL,
    access INTEGER NOT NULL,
    access_between TEXT,
    expires_on INTEGER,
    FOREIGN KEY (zone_id) REFERENCES Zone(zone_id),
    FOREIGN KEY (user_id) REFERENCES User(user_id)
);

CREATE TABLE Door (
    door_id TEXT UNIQUE,
    zone_id INTEGER NOT NULL,
    PRIMARY KEY (door_id),
    FOREIGN KEY (zone_id) REFERENCES Zone(zone_id)
);

CREATE TABLE Event_Log (
    event_id INTEGER,
    door_id TEXT NOT NULL,
    user_id TEXT,
    time_stamp INTEGER NOT NULL,
    zone_authorised INTEGER NOT NULL,
    key_authorised INTEGER NOT NULL,
    PRIMARY KEY (event_id),
    FOREIGN KEY (door_id) REFERENCES Door(door_id),
    FOREIGN KEY (user_id) REFERENCES User(user_id)
);
